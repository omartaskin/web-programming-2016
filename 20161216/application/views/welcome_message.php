<?php


$this->load->library('table');

$data = array(
    array('Name', 'Color', 'Size'),
    array('Fred', 'Blue', 'Small'),
    array('Mary', 'Red', 'Large'),
    array('John', 'Green', 'Medium')
);

echo $this->table->generate($data);

?>