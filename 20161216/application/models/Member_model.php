<?php

/**
 * Created by IntelliJ IDEA.
 * User: otaskin
 * Date: 16/12/2016
 * Time: 13:54
 */
class Member_model extends CI_Model
{

    public function getAllMember()
    {
        // SELECT * FROM member
        $this->db->get('member');
    }

    public function save($name, $email, $password)
    {
        $this->db->insert(
            'member',
            array(
                'NAME' => $name,
                'EMAIL' => $email,
                'PASSWORD' => $password,
            )
        );
    }
}